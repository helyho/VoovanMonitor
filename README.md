![](http://git.oschina.net/uploads/images/2016/0510/122514_7d971a34_116083.jpeg)
=======================================================================================
#####基于 Voovan 开发的用于监控 Voovan 框架的性能信息.
#####这是一个 Voovan 项目的模块,需要在 Voovan HttpServer项目中添加该模块.

```json
  //配置文件: conf/web.json
  "Modules": [
    {
      "Name": "性能监控模块",                                      //模块名称
      "Path": "/VoovanMonitor",                                    //模块路径
      "ClassName": "org.voovan.http.monitor.Monitor"               //模块处理器
    }
  ]
```
- [演示地址](http://webserver.voovan.org/VoovanMonitor/Monitor.html)
演示站点服务器在国外,所以 chart 表刷新可能会有延迟.

----------------------

- 关于部署
   - 部署的时候需要将 WEBAPP 目录覆盖到目标 VoovanHttpServer 的 WEBAPP 目录
   - 将VoovanMonitor.jar复制到到目标 VoovanHttpServer 的 lib 目录
   - 并按照上面的模块配置在 web.json 中进行配置
   - 访问: http://x.x.x.x/VoovanMonitorr/Monitor.html
   
- 快捷部署
   - 通过 `git clone https://git.oschina.net/helyho/VoovanMonitor.git`
   - 运行 start.sh
 

----------------------

- 概要信息
![概要信息](http://git.oschina.net/uploads/images/2016/0510/172248_ce66c996_116083.png "概要信息")

----------------------
- 系统性能
![CPU](http://git.oschina.net/uploads/images/2016/0510/172426_e0f9c9de_116083.png "CPU")
![内存](http://git.oschina.net/uploads/images/2016/0510/172545_eb4c6fab_116083.png "内存")
![线程和对象](http://git.oschina.net/uploads/images/2016/0510/172631_8c6a389e_116083.png "线程和对象")
----------------------
- JVM 对象信息
![JVM 对象信息](http://git.oschina.net/uploads/images/2016/0510/172704_99e17c2f_116083.png "JVM 对象信息")
----------------------
- 线程信息
![线程信息](http://git.oschina.net/uploads/images/2016/0510/172728_06db8325_116083.png "线程信息")
![栈信息](http://git.oschina.net/uploads/images/2016/0510/173222_b1f9a423_116083.png "栈信息")
----------------------
- 请求性能分析
![请求性能分析](http://git.oschina.net/uploads/images/2016/0510/172815_7055d9cf_116083.png "请求性能分析")
----------------------
- 日志信息
![日志信息](http://git.oschina.net/uploads/images/2016/0510/172847_7ac73a5d_116083.png "日志信息")
![系统日志](http://git.oschina.net/uploads/images/2016/0510/172913_922755de_116083.png "系统日志")
