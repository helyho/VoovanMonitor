package org.voovan.http.monitor;

import org.voovan.http.server.HttpModule;
import org.voovan.http.server.context.HttpFilterConfig;

/**
 * 监控器类
 *
 * @author helyho
 *
 * Java Framework.
 * WebSite: https://github.com/helyho/Voovan
 * Licence: Apache v2 License
 */
public class Monitor extends HttpModule {

    @Override
    public void install() {
        get("/api/:Type/:Param1",new MonitorHandler());
        get("/api/:Type",new MonitorHandler());
        get("/api/:Type/:Param1/:Param2",new MonitorHandler());
        filterChain().add(HttpFilterConfig.newInstance("MonitorFilter",HttpMonitorFilter.class,null));
    }

    @Override
    public void unInstall() {

    }
}
